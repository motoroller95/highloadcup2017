class CreateVisits < ActiveRecord::Migration[5.2]
  def change
    create_table :visits do |t|
      t.belonts_to :location
      t.belonts_to :user
      t.timestamp :visited_at
      t.integer :mark

      t.timestamps
    end
  end
end
