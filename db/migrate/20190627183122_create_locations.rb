class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.string :place
      t.string :country
      t.string :city
      t.integer :distance

      t.timestamps
    end
  end
end
